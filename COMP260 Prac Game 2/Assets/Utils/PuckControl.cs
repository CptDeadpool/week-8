﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	private AudioSource audio;
	public LayerMask paddleLayer;

	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	void OnCollisionEnter(Collision collision) {

		if (paddleLayer.Contains(collision.gameObject)) {
			
		audio.PlayOneShot(paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}

	

	// Update is called once per frame
	void Update () {

	}
}

